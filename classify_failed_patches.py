#!/usr/bin/python3

# License: Expat (MIT)
# Copyright: 2018 Daniel Sangorrin

import argparse
import sys
import os
import git # sudo apt-get install python-git
import re
from collections import defaultdict

# Usage:
# $ ./classify_failed_patches.py --file subjects.txt --kernel /path/to/4.4.y [--refname stable-rc/4.4.y]
#  -> subjects.txt lines format: ^[PATCH] subject..
#  -> other lines will be printed but not parsed
# $ tail -f toapply.txt
# $ tail -f applied.txt
parser = argparse.ArgumentParser(description='Classify failed patches')
parser.add_argument('-f', '--file', type=argparse.FileType('r', encoding='utf-8'), required=True, help='list of patch subjects starting with [PATCH]')
parser.add_argument('-k', '--kernelpath', type=str, required=True, help='kernel folder to check')
parser.add_argument('-r', '--refname', type=str, default="HEAD", help='kernel ref name to check')
args = parser.parse_args()

# Get branch version from refname (stable-rc/linux-4.19.y)
branch = args.refname.split('/')[-1].split('-')[-1].replace('.y', '')

# Read in the prefix matching pattern
with open('prefix-match-pattern') as pattern:
    pattern = pattern.readline().rstrip()
    # replace v?[345]\.[1-9][0-9]* with branch version
    pattern = pattern.replace('v?[345]\.[1-9][0-9]*', 'v?' + branch.replace('.', '\.'))
    patch_prefix_re = re.compile(pattern, re.IGNORECASE)

# All sorts of annoying tags after [PATCH]
patch_tag_re = re.compile(r"""\[(
        (?# various common tags )
        bug(fix)? |
        stable |
        stable-resend |
        resend |
        rfc |
        net |
        net-next\?? (?# why did someone put a ? in their tag...) |
        (?# RFC and patch revision tags )
        (RFC\s*)?[vV][1-9][0-9]* |
        (?# The next one covers most of the stable backport tags )
        ((stable(\ backport)?|Backport\ to\ stable)\ )?(v?[345]\.[0-9]+(\.y)?(-(ckt|stable))?([/,]\ ?)?)* |
        (?# Another backport approach )
        request\ for\ stable\ v?[345].[0-9]+\ inclusion |
        (?# This is another kind of backport )
        stable-(v?[345]\.[0-9]+(\.y)?([/,]\ ?)?)+
    )\s*]\s*""", re.IGNORECASE | re.VERBOSE)

commits = defaultdict(list)
patches = dict()
repo = git.Git(args.kernelpath)
log = repo.log('--pretty=format:%H %s', '--no-decorate', '--no-merges', args.refname).splitlines()
for line in log:
    (commit, subject) = line.split(' ', 1)
    commits[subject].append(commit)

with open('applied.txt', 'a+', encoding='utf-8') as applied, \
        open('toapply.txt', 'a+', encoding='utf-8') as toapply:
    for line in args.file:
        line = line.rstrip()

        if patch_prefix_re.match(line):
            subject = patch_prefix_re.sub('', line)
            subject = patch_tag_re.sub('', subject)

            # Skip repeated patches, possibly because of different patch
            # revisions or separately backported for different versions.
            if subject in patches:
                continue
            patches[subject] = True

            print (subject)
            if subject not in commits:
                msg = '[TOAPPLY] %s' % (subject)
                print (msg)
                toapply.write(msg + '\n')
            else:
                msg = '[APPLIED] %s' % (subject)
                print (msg)
                applied.write(msg + '\n')
                exists = '\n\t'.join(('%s %s' % (commit, subject) for commit in commits[subject]))
                print ('\t' + exists)
                applied.write('\t' + exists + '\n')
        else:
            # Patch not matched, likely not targeting the branch we are looking at
            continue
        # for tail -f toapply.txt to work
        toapply.flush()
        applied.flush()
