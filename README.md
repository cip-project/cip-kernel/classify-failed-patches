## How to use

The directory name for the linux tree was chosen to match the one used
in cip-kernel-sec, so they can share the same copy.

1. clone this repository
```shell
git clone https://gitlab.com/cip-project/cip-kernel/classify-failed-patches.git
```
2. clone linux tree
```shell
git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git kernel
cd kernel
git remote add -f stable https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
git remote add -f stable-rc https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
cd ..
```
3. clone Mailing list data of stable 
```shell
git clone https://git.kernel.org/pub/scm/public-inbox/vger.kernel.org/stable/0.git linux-stable-ml
```
4. change directory to classify-failed-patches
```shell
cd classify-failed-patches
```
5. Run make
```shell
make
```
