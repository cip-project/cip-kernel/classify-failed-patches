#! /usr/bin/make -f
# -*- makefile -*-

KERNEL:=../kernel
STABLE_ML:=../linux-stable-ml
TARGETVER:=4.4.y 4.19.y
MAKEFILE_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
PREFIX_MATCH_PATTERN := $(file < prefix-match-pattern)
 
define check_patches
 	cd ${KERNEL}; \
 	git remote update stable-rc ; \
 	cd - \
 
 	./classify_failed_patches.py --file patch-list --kernel ${KERNEL} --refname stable-rc/linux-$1
 
 	mv applied.txt $1.applied.txt
 	mv toapply.txt $1.toapply.txt 
endef
 
.PHONY: show-unmatched-patches update-patch-list $(TARGETVER)

all: $(TARGETVER)

show-unmatched-patches:
	cd ${STABLE_ML}; git pull; git log --pretty='%s' | \
	    grep -a "^\[PATCH" | \
	    grep -a -v -i -E "${PREFIX_MATCH_PATTERN}" | \
	    grep -a -v "\[GIT PULL" | uniq --unique

update-patch-list:
	cd ${STABLE_ML}; git pull; git log --pretty='%s' | \
	    grep -a -i -E "${PREFIX_MATCH_PATTERN}" | \
	    grep -a -v "\[GIT PULL" | uniq --unique > $(PWD)/patch-list

4.4.y: ${REPO} update-patch-list
	$(call check_patches,$@)

4.19.y: ${REPO} update-patch-list
	$(call check_patches,$@)
